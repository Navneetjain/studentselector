package com.example.navneet.studentselector;

import java.util.List;

class StudentEnrollmentModel {
    String name, gender, grade;
    List<String> subject_ids;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public List<String> getSubject_ids() {
        return subject_ids;
    }

    public void setSubject_ids(List<String> subject_ids) {
        this.subject_ids = subject_ids;
    }
}
