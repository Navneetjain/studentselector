package com.example.navneet.studentselector;

import java.util.List;

class StudentEnrollmentWithValidationModel {
    String name, gender, grade;
    boolean nameError, genderError, gradeError, subjectError;
    List<String> subject_ids;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public boolean isNameError() {
        return nameError;
    }

    public void setNameError(boolean nameError) {
        this.nameError = nameError;
    }

    public boolean isGenderError() {
        return genderError;
    }

    public void setGenderError(boolean genderError) {
        this.genderError = genderError;
    }

    public boolean isGradeError() {
        return gradeError;
    }

    public void setGradeError(boolean gradeError) {
        this.gradeError = gradeError;
    }

    public boolean isSubjectError() {
        return subjectError;
    }

    public void setSubjectError(boolean subjectError) {
        this.subjectError = subjectError;
    }

    public List<String> getSubject_ids() {
        return subject_ids;
    }

    public void setSubject_ids(List<String> subject_ids) {
        this.subject_ids = subject_ids;
    }
}
