package com.example.navneet.studentselector;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ChildInfoAdapter extends RecyclerView.Adapter<ChildInfoAdapter.MyViewHolder> {
    private Context context;
    private List<StudentEnrollmentWithValidationModel> studentEnrollmentModelList;
    private GetStudentInfoForEnrollmentListener listener;
    private int lastPosition = -1;

    public ChildInfoAdapter(Context context1, List<StudentEnrollmentWithValidationModel> studentEnrollmentModelList1, GetStudentInfoForEnrollmentListener listener1) {
        this.context = context1;
        this.studentEnrollmentModelList = studentEnrollmentModelList1;
        this.listener = listener1;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        final LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        final View view = layoutInflater.inflate(R.layout.child_info_adapter, parent, false);
        MyViewHolder mvh = new MyViewHolder(view);
        return mvh;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        StudentEnrollmentWithValidationModel model = studentEnrollmentModelList.get(position);

        if (model.getName() != null){
            holder.mStudentNameEt.setText(model.getName());
        }

        if (model.getGender().equals("male")) {
            holder.genderGroup.check(R.id.male);
        } else if (model.getGender().equals("female")) {
            holder.genderGroup.check(R.id.female);
        } else if (model.getGender().equals("others")) {
            holder.genderGroup.check(R.id.others);
        }

        switch (model.getGrade()){
            case "first":
                holder.gradeRadioGroup.check(R.id.grade1);
                break;
            case "second":
                holder.gradeRadioGroup.check(R.id.grade2);
                break;
            case "third":
                holder.gradeRadioGroup.check(R.id.grade3);
                break;
            case "fourth":
                holder.gradeRadioGroup.check(R.id.grade4);
                break;
            case "fifth":
                holder.gradeRadioGroup.check(R.id.grade5);
                break;
            case "sixth":
                holder.gradeRadioGroup.check(R.id.grade6);
                break;
            case "seventh":
                holder.gradeRadioGroup.check(R.id.grade7);
                break;
            default:
                break;
        }

        if (model.isNameError()){
            holder.nameErrorTv.setVisibility(View.VISIBLE);
            holder.nameErrorTv.setText("Please Enter Name");
        }else {
            holder.nameErrorTv.setVisibility(View.GONE);
        }

        if (model.isGradeError()){
            holder.gradeErrorTv.setVisibility(View.VISIBLE);
            holder.gradeErrorTv.setText("Please Select Grade");
        }else {
            holder.gradeErrorTv.setVisibility(View.GONE);
        }

        if (model.isGenderError()){
            holder.genderErrorTv.setVisibility(View.VISIBLE);
            holder.genderErrorTv.setText("Please Select Gender");
        }else {
            holder.genderErrorTv.setVisibility(View.GONE);
        }

        holder.mStudentNameEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                holder.nameErrorTv.setVisibility(View.GONE);
                listener.setStudentName(position, holder.mStudentNameEt.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        holder.genderGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = group.getCheckedRadioButtonId();
                RadioButton radioButton = group.findViewById(selectedId);

                switch (radioButton.getText().toString()) {
                    case "Male":
                        listener.setStudentGender(position, "male");
                        break;
                    case "Female":
                        listener.setStudentGender(position, "female");
                        break;
                    case "Others":
                        listener.setStudentGender(position, "other");
                        break;
                    default:
                        listener.setStudentGender(position, "male");
                }
            }
        });

        holder.gradeRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int selectedId = group.getCheckedRadioButtonId();
                RadioButton radioButton = group.findViewById(selectedId);

                switch (radioButton.getText().toString()) {
                    case "1":
                        listener.setStudentGrade(position, "first");
                        break;
                    case "2":
                        listener.setStudentGrade(position, "second");
                        break;
                    case "3":
                        listener.setStudentGrade(position, "third");
                        break;
                    case "4":
                        listener.setStudentGrade(position, "fourth");
                        break;
                    case "5":
                        listener.setStudentGrade(position, "fifth");
                        break;
                    case "6":
                        listener.setStudentGrade(position, "sixth");
                        break;
                    case "7":
                        listener.setStudentGrade(position, "seventh");
                        break;
                    default:
                        listener.setStudentGrade(position, "first");
                }
            }
        });

        setAnimation(holder.itemView, position);
    }


    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        return studentEnrollmentModelList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.mStudentNameEt)
        TextInputEditText mStudentNameEt;
        @BindView(R.id.genderGroup)
        RadioGroup genderGroup;
        @BindView(R.id.nameErrorTv)
        TextView nameErrorTv;
        @BindView(R.id.genderErrorTv)
        TextView genderErrorTv;
        @BindView(R.id.gradeErrorTv)
        TextView gradeErrorTv;
        @BindView(R.id.gradeRadioGroup)
        RadioGroup gradeRadioGroup;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
