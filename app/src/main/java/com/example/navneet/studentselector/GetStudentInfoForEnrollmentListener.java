package com.example.navneet.studentselector;

public interface GetStudentInfoForEnrollmentListener {
    void setStudentName(int index, String name);
    void setStudentGrade(int index, String grade);
    void setStudentGender(int index, String gender);
}
