package com.example.navneet.studentselector;

import java.util.List;

class ParentEnrollmentModel {
    String name, gender, email, latitude, longitude;
    List<StudentEnrollmentModel> students;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public List<StudentEnrollmentModel> getStudents() {
        return students;
    }

    public void setStudents(List<StudentEnrollmentModel> students) {
        this.students = students;
    }
}
