package com.example.navneet.studentselector;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements GetStudentInfoForEnrollmentListener{
    @BindView(R.id.childRv)
    RecyclerView childRv;
    @BindView(R.id.enrollButton)
    Button enrollButton;
    @BindView(R.id.incrementStudentButton)
    ImageView incrementStudentButton;
    @BindView(R.id.noOfStudentValueTv)
    TextView noOfStudentValueTv;

    @BindView(R.id.decrementStudentButton)
    ImageView decrementStudentButton;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    private List<StudentEnrollmentWithValidationModel> studentEnrollmentList;
    private List<StudentEnrollmentModel> finalStudentsEnrollmentList;
    private List<String> subjectList;

    private ChildInfoAdapter childInfoAdapter;

    private StudentEnrollmentModel finalStudentEnrollmentModel;
    private StudentEnrollmentWithValidationModel studentEnrollmentModel;

    private ParentEnrollmentModel parentEnrollmentModel;

    private InputMethodManager inputManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        subjectList = new ArrayList<>();
        subjectList.add("1");
        subjectList.add("2");

        parentEnrollmentModel = new ParentEnrollmentModel();

        studentEnrollmentList = new ArrayList<>();
        finalStudentsEnrollmentList = new ArrayList<>();

        studentEnrollmentModel = new StudentEnrollmentWithValidationModel();
        studentEnrollmentModel.setGrade("");
        studentEnrollmentModel.setName("");
        studentEnrollmentModel.setGender("");
        studentEnrollmentModel.setSubject_ids(subjectList);

        studentEnrollmentList.add(studentEnrollmentModel);

        initRecyclerView();

        incrementStudentButton.setOnClickListener(incrementStudentListener);
        decrementStudentButton.setOnClickListener(decrementStudentListener);

        enrollButton.setOnClickListener(enrollmentListener);
    }


    private void initRecyclerView() {
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        childRv.setLayoutManager(layoutManager);
        childRv.setHasFixedSize(true);
        childRv.setNestedScrollingEnabled(false);
        childInfoAdapter = new ChildInfoAdapter(this, studentEnrollmentList, this);
        childRv.setAdapter(childInfoAdapter);
    }


    View.OnClickListener incrementStudentListener = new View.OnClickListener() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onClick(View view) {

            decrementStudentButton.setVisibility(View.VISIBLE);

            studentEnrollmentModel = new StudentEnrollmentWithValidationModel();
            studentEnrollmentModel.setGrade("");
            studentEnrollmentModel.setName("");
            studentEnrollmentModel.setGender("");
            studentEnrollmentModel.setSubject_ids(subjectList);

            studentEnrollmentList.add(studentEnrollmentModel);

            childInfoAdapter.notifyDataSetChanged();

            noOfStudentValueTv.setText("2");
            incrementStudentButton.setVisibility(View.GONE);

        }
    };

    View.OnClickListener decrementStudentListener = new View.OnClickListener() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onClick(View view) {

            incrementStudentButton.setVisibility(View.VISIBLE);
            studentEnrollmentList.remove(1);
            childInfoAdapter.notifyItemRemoved(1);

            noOfStudentValueTv.setText("1");
            decrementStudentButton.setVisibility(View.GONE);

            Log.d("DEBUG", "list size = " + studentEnrollmentList.size());
        }
    };


    @Override
    public void setStudentName(int index, String name) {
        studentEnrollmentList.get(index).setName(name);
    }

    @Override
    public void setStudentGrade(int index, String grade) {
        studentEnrollmentList.get(index).setGrade(grade);
    }

    @Override
    public void setStudentGender(int index, String gender) {
        studentEnrollmentList.get(index).setGender(gender);
        enrollButton.setVisibility(View.VISIBLE);
    }

    View.OnClickListener enrollmentListener = new View.OnClickListener() {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @SuppressLint("SetTextI18n")
        @Override
        public void onClick(View view) {
            boolean isEmptyField = false;
            int counter = 0;

            inputManager.hideSoftInputFromWindow(Objects.requireNonNull(getCurrentFocus()).getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

            for (int i = 0; i < studentEnrollmentList.size(); i++) {
                isEmptyField = checkValidation(studentEnrollmentList.get(i), i);

                if (isEmptyField) {
                    counter = counter + 1;
                    childInfoAdapter.notifyItemChanged(i);
                }
            }

            if (counter == 0){
                for (int i = 0; i < studentEnrollmentList.size(); i++) {
                    finalStudentEnrollmentModel = new StudentEnrollmentModel();
                    finalStudentEnrollmentModel.setName(studentEnrollmentList.get(i).getName());
                    finalStudentEnrollmentModel.setGender(studentEnrollmentList.get(i).getGender());
                    finalStudentEnrollmentModel.setGrade(studentEnrollmentList.get(i).getGrade());
                    finalStudentEnrollmentModel.setSubject_ids(studentEnrollmentList.get(i).getSubject_ids());
                    finalStudentsEnrollmentList.add(finalStudentEnrollmentModel);
                }

                parentEnrollmentModel.setStudents(finalStudentsEnrollmentList);

            }
        }
    };

    private boolean checkValidation(StudentEnrollmentWithValidationModel model, int position) {
        int counter = 0;

        if (model.getGender().equals("")) {
            studentEnrollmentList.get(position).setGenderError(true);
            counter = counter + 1;
        } else {
            studentEnrollmentList.get(position).setGender(model.getGender());
            studentEnrollmentList.get(position).setGenderError(false);
        }

        if (model.getGrade().equals("")) {
            studentEnrollmentList.get(position).setGradeError(true);
            counter = counter + 1;
        } else {
            studentEnrollmentList.get(position).setGrade(model.getGrade());
            studentEnrollmentList.get(position).setGradeError(false);
        }
        if (model.getName().equals("")) {
            studentEnrollmentList.get(position).setNameError(true);
            counter = counter + 1;
        } else {
            studentEnrollmentList.get(position).setName(model.getName());
            studentEnrollmentList.get(position).setNameError(false);
        }

        return counter > 0;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
